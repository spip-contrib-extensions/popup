<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/spipopup?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'btn_fermer_fenetre' => 'Close',
	'btn_fermer_fenetre_ttl' => 'Close this window',
	'btn_haut_page' => 'Top of the page',
	'btn_haut_page_ttl' => 'Back to the top of the page',
	'btn_imprimer' => 'Print',
	'btn_imprimer_ttl' => 'Print this page',

	// D
	'descr_cfg' => 'Plugin documentation, for information : [spip-contrib.net/?article3573->http://contrib.spip.net/?article3573]', # MODIF

	// E
	'editor' => 'Popup link editor',
	'editor_enregistrer' => 'Save',
	'editor_erreur_entree_obligatoire' => 'This input is mandatory !',
	'editor_lien' => 'Link to insert (URL or SPIP object)',
	'editor_skel' => 'SPIP page skeleton',
	'editor_texte' => 'Text of the link',
	'editor_titre' => 'Title of the link (on mouseover)',
	'editor_titre_inserer_lien_popup' => 'Insert a popup link',

	// H
	'height' => 'Height (in pixels)',

	// I
	'inserer_lien_popup' => 'Insert a link in a popup window',

	// L
	'legend_cfg_balise' => 'Concerning the external window',

	// N
	'nom' => '#POPUP Tag',
	'nouvelle_fenetre' => '[New window]',

	// O
	'options_popup' => 'JavaScript options  of the new window',
	'options_popup_comment' => 'Here you can indicate an option table for the new window with the following shape: "variable1: value1, variable2: value2, ..." ({ex.: "location: 0, scrollbars: 1"}).',

	// P
	'popup_titre' => 'Chatbox',

	// R
	'retour_fenetre' => '[Back to the main window]',

	// S
	'skel_defaut' => 'Default skeleton used to display the window content',
	'skel_defaut_comment' => 'It is initialy the skeleton "popup_defaut.html" at the website root.',
	'spipopup' => 'SPIPopup',

	// T
	'titre_descr_cfg' => '"Popup" configuration',
	'titre_popup' => 'JavaScript name of the window',
	'titre_popup_comment' => 'You can then use this name to call it in your scripts ("window.popup").',

	// W
	'width' => 'Width (in pixels)',
	'width_and_height' => 'Window size'
);
