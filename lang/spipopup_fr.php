<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/popup.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'btn_fermer_fenetre' => 'Fermer',
	'btn_fermer_fenetre_ttl' => 'Fermer cette fenêtre',
	'btn_haut_page' => 'Haut de page',
	'btn_haut_page_ttl' => 'Retour en haut de page',
	'btn_imprimer' => 'Imprimer',
	'btn_imprimer_ttl' => 'Imprimer cette page',

	// D
	'descr_cfg' => 'Documentation du plugin pour plus d’infos : [contrib.spip.net/ ?article3573->https://contrib.spip.net/?article3573]',

	// E
	'editor' => 'Editeur de lien popup',
	'editor_enregistrer' => 'Enregistrer',
	'editor_erreur_entree_obligatoire' => 'Cette entrée est obligatoire !',
	'editor_lien' => 'Lien à insérer (URL ou objet SPIP)',
	'editor_skel' => 'Squelette de page SPIP',
	'editor_texte' => 'Texte du lien',
	'editor_titre' => 'Titre du lien (au passage de la souris)',
	'editor_titre_inserer_lien_popup' => 'Insérer un lien popup',

	// H
	'height' => 'Hauteur (en pixels)',

	// I
	'inserer_lien_popup' => 'Insérer un lien en fenêtre popup',

	// L
	'legend_cfg_balise' => 'Concernant la fenêtre externe',

	// N
	'nom' => 'Balise #POPUP',
	'nouvelle_fenetre' => '[Nouvelle fenêtre]',

	// O
	'options_popup' => 'Options JavaScript passées à la nouvelle fenêtre',
	'options_popup_comment' => 'Indiquez ici un tableau d’options qui seront passées à la nouvelle fenêtre sous la forme d’un tableau du genre : "variable1 : valeur1, variable2 : valeur2, ..." ({ex. : "location : 0, scrollbars : 1"}).',

	// P
	'popup_titre' => 'Boîte de dialogue',

	// R
	'retour_fenetre' => '[Retour en fenêtre principale]',

	// S
	'skel_defaut' => 'Squelette par défaut utilisé pour afficher le contenu de la fenêtre',
	'skel_defaut_comment' => 'Il s’agit initialement du squelette "popup_defaut.html" présent à la racine du plugin.',
	'spipopup' => 'SPIPopup',

	// T
	'titre_descr_cfg' => 'Configuration de l’outil "Popup"',
	'titre_popup' => 'Nom JavaScript de la fenêtre',
	'titre_popup_comment' => 'Vous pourrez par la suite utiliser ce nom pour la désigner dans vos scripts ("window.popup").',

	// W
	'width' => 'Largeur (en pixels)',
	'width_and_height' => 'Taille de la fenête'
);
