<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/spipopup?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'btn_fermer_fenetre' => 'Sluiten',
	'btn_fermer_fenetre_ttl' => 'Dit venster sluiten',
	'btn_haut_page' => 'Begin pagina',
	'btn_haut_page_ttl' => 'Naar het begin van de pagina',
	'btn_imprimer' => 'Afdrukken',
	'btn_imprimer_ttl' => 'Deze pagina afdrukken',

	// D
	'descr_cfg' => 'Documentatie van de plugin voor meer informatie: [spip-contrib.net/ ?article3573->https://contrib.spip.net/?article3573]',

	// E
	'editor' => 'Popup link aanpassen',
	'editor_enregistrer' => 'Opslaan',
	'editor_erreur_entree_obligatoire' => 'Deze gegevens zijn verplicht!',
	'editor_lien' => 'In te voegen link (URL of SPIP object)',
	'editor_skel' => 'SPIP skelet voor de pagina',
	'editor_texte' => 'Tekst van de link',
	'editor_titre' => 'Titel van de link (wanneer er met muis overheen wordt gegaan)',
	'editor_titre_inserer_lien_popup' => 'Een popup link invoegen',

	// H
	'height' => 'Hoogte (in pixels)',

	// I
	'inserer_lien_popup' => 'Een link als popup venster invoegen',

	// L
	'legend_cfg_balise' => 'Betreft een extern venster',

	// N
	'nom' => 'Baken #POPUP',
	'nouvelle_fenetre' => '[Nieuw venster]',

	// O
	'options_popup' => 'Aan het nieuwe venster doorgegeven JavaScript opties',
	'options_popup_comment' => 'Vermeld hier een tabel van opties die aan het nieuwe venster moeten worden doorgegeven in de vorm: "variabele1 : waarde1, variabele2 : waarde2, ..." ({bv.: "location : 0, scrollbars : 1"}).',

	// P
	'popup_titre' => 'Dialoogbox',

	// R
	'retour_fenetre' => '[Terug naar het hoofdvenster]',

	// S
	'skel_defaut' => 'Standaard skelet gebruikt voor de weergave van de inhoud van het venster',
	'skel_defaut_comment' => 'Het gaat hier om het skelet "popup_defaut.html" aanwezig in de root van de plugin.',
	'spipopup' => 'SPIPopup',

	// T
	'titre_descr_cfg' => 'Configuratie van de "Popup" tool',
	'titre_popup' => 'Naam JavaScript van het venster',
	'titre_popup_comment' => 'Je mag in het vervolg deze naam gebruiken in je ("window.popup") scripts.',

	// W
	'width' => 'Breedte (in pixels)',
	'width_and_height' => 'Afmetingen van het venster'
);
