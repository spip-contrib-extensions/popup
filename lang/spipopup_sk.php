<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/spipopup?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'btn_fermer_fenetre' => 'Zatvoriť',
	'btn_fermer_fenetre_ttl' => 'Zatvoriť toto okno',
	'btn_haut_page' => 'Vrchná časť stránky',
	'btn_haut_page_ttl' => 'Návrat na vrchnú časť stránky',
	'btn_imprimer' => 'Vytlačiť',
	'btn_imprimer_ttl' => 'Vytlačiť túto stránku',

	// D
	'descr_cfg' => 'Dokumentácia zásuvného modulu pre viac informácií: [spip-contrib.net/?article3573->http://contrib.spip.net/?article3573]', # MODIF

	// E
	'editor' => 'Upraviť vyskakovací odkaz',
	'editor_enregistrer' => 'Uložiť',
	'editor_erreur_entree_obligatoire' => 'Tento údaj je povinný!',
	'editor_lien' => 'Odkaz, ktorý chcete vložiť (adresa alebo objekt SPIPu)',
	'editor_skel' => 'Šablóna stránky v SPIPe',
	'editor_texte' => 'Text odkazu',
	'editor_titre' => 'Nadpis odkazu (pri prejdení myšou)',
	'editor_titre_inserer_lien_popup' => 'Vložiť vyskakovací odkaz',

	// H
	'height' => 'Výška (v pixeloch)',

	// I
	'inserer_lien_popup' => 'Vložiť odkaz vo vyskakovacom okne',

	// L
	'legend_cfg_balise' => 'O externom okne',

	// N
	'nom' => 'Tag #POPUP',
	'nouvelle_fenetre' => '[Nové okno]',

	// O
	'options_popup' => 'Nastavenia JavaScriptu, ktoré prešli na nové okno',
	'options_popup_comment' => 'Tu zadajte pole s nastaveniami, ktoré prejdú na nové okno vo forme tabuľky v podobe: "premenná1: hodnota1, premenná2: hodnota2, ..." ({príkl.: "poloha: 0, rolovacie pruhy: 1"}).',

	// P
	'popup_titre' => 'Dialógové okno',

	// R
	'retour_fenetre' => '[Späť na hlavné okno]',

	// S
	'skel_defaut' => 'Predvolená šablóna, ktorá sa použije na zobrazenie obsahu okna',
	'skel_defaut_comment' => 'Toto je začiatok šablóny "popup_defaut.html", ktorá sa nachádza v koreňovom adresári zásuvného modulu.',
	'spipopup' => 'SPIPopup',

	// T
	'titre_descr_cfg' => 'Nastavenia nástroja "Vyskakovacie okno"',
	'titre_popup' => 'Názov JavaScriptu okna',
	'titre_popup_comment' => 'Na označenie v svojich skriptoch ("window.popup") môžete použiť tento názov.',

	// W
	'width' => 'Šírka (v pixeloch)',
	'width_and_height' => 'Veľkosť okna'
);
